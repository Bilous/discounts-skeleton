# Discounts functionality

Discounts functionality is a project for showing frontend skills.

Application receives list of products and calculate available discount. 

All data is mocked due to the fact that we dont use backend or third-party service. 
Mocked data in a mocks folder.

PropTypes need to be added in the future. It was not implemented for saving time.

## Structure

Current solution use a simple folder structure. 
Bigger project should be divided into modules.
It means that each entity should have separate folder and contain own actions, reducers, components etc.

## Styles

Used BEM methodology. 
For future improvement i would configure scss or styled components.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
