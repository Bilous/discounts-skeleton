import { createAction } from "redux-actions";
import { ProductsServiceInstance } from "../services/ProductsService";

export const ProductActionTypes = {
  FETCH_PRODUCTS_START: "FETCH_PRODUCTS_START",
  FETCH_PRODUCTS_SUCCESS: "FETCH_PRODUCTS_SUCCESS",
  FETCH_PRODUCTS_FAILURE: "FETCH_PRODUCTS_FAILURE"
};

export const productsRequestStart = createAction(
  ProductActionTypes.FETCH_PRODUCTS_START
);
export const productsRequestSuccess = createAction(
  ProductActionTypes.FETCH_PRODUCTS_SUCCESS
);
export const productsRequestFailure = createAction(
  ProductActionTypes.FETCH_PRODUCTS_FAILURE
);

export function fetchProducts() {
  return function(dispatch) {
    dispatch(productsRequestStart());
    return ProductsServiceInstance.getProducts()
      .then(response => {
        dispatch(productsRequestSuccess(response));
      })
      .catch(error => {
        console.error(error);
        dispatch(productsRequestFailure());
      });
  };
}
