import { createAction } from "redux-actions";

export const CartActionTypes = {
  ADD_TO_CART: "ADD_TO_CART",
  REMOVE_FROM_CART: "REMOVE_FROM_CART",
};

export const addToCart = createAction(CartActionTypes.ADD_TO_CART);

export const removeFromCart = createAction(CartActionTypes.REMOVE_FROM_CART);
