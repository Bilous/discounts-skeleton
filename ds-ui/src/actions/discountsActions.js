import { createAction } from "redux-actions";
import { DiscountsServiceInstance} from "../services/DiscountsService";

export const DiscountsActionTypes = {
  FETCH_DISCOUNTS_START: "FETCH_DISCOUNTS_START",
  FETCH_DISCOUNTS_SUCCESS: "FETCH_DISCOUNTS_SUCCESS",
  FETCH_DISCOUNTS_FAILURE: "FETCH_DISCOUNTS_FAILURE"
};

export const discountsRequestStart = createAction(
  DiscountsActionTypes.FETCH_DISCOUNTS_START
);
export const discountsRequestSuccess = createAction(
  DiscountsActionTypes.FETCH_DISCOUNTS_SUCCESS
);
export const discountsRequestFailure = createAction(
  DiscountsActionTypes.FETCH_DISCOUNTS_FAILURE
);

export function fetchDiscounts() {
  return function(dispatch) {
    dispatch(discountsRequestStart());
    return DiscountsServiceInstance.getDiscounts()
      .then(response => {
        dispatch(discountsRequestSuccess(response));
      })
      .catch(error => {
        console.error(error);
        dispatch(discountsRequestFailure());
      });
  };
}
