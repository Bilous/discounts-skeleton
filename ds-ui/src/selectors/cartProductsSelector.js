export const cartProductsSelector = state => {
  const { products, cart } = state;

  return products.items.reduce((totalProducts, product) => {
    if (cart.items[product.id]) {
      totalProducts.push({ ...product, quantity: cart.items[product.id] });
    }
    return totalProducts;
  }, []);
};
