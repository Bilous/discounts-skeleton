export const cartItemsQuantitySelector = state =>
  Object.values(state.cart.items).reduce((acc, cur) => acc + cur, 0);
