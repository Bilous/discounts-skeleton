export const discounts = [
  {
    id: 1,
    attributes: {
      startDate: new Date("05/04/2020 00:00:00"),
      endDate: new Date("05/15/2020 00:00:00")
    },
    productId: 4,
    productName: 'Apples',
    discountAmount: 0.1
  },
  {
    id: 2,
    attributes: {
      conditions: [{ targetProductId: 1, targetProductName: 'Soup', targetProductCount: 2 }]
    },
    productId: 2,
    productName: 'Bread',
    discountAmount: 0.5
  },

  // This is complex condition for demonstrating all functionality.
  {
    id: 3,
    attributes: {
      startDate: new Date("05/04/2020 00:00:00"),
      endDate: new Date("05/22/2020 00:00:00"),
      conditions: [
        // we can use many conditions.
        { targetProductId: 4, targetProductName: 'Apples', targetProductCount: 5 },
        { targetProductId: 1, targetProductName: 'Soup', targetProductCount: 2 }
      ]
    },
    productId: 3,
    productName: 'Milk',
    discountAmount: 0.25
  }
];
