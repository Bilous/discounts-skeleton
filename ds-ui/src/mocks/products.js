export const products = [
  {
    id: 1,
    name: "Soup",
    price: 0.65,
    img: "soup"
  },
  {
    id: 2,
    name: "Bread",
    price: 0.8,
    img: "bread"
  },
  {
    id: 3,
    name: "Milk",
    price: 1.3,
    img: "milk"
  },
  {
    id: 4,
    name: "Apples",
    price: 1,
    img: "apples"
  }
];
