import { products } from "../mocks/products";

export class ProductsService {
  getProducts = () => Promise.resolve(products);
}
export const ProductsServiceInstance = new ProductsService();