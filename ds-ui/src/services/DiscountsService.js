import { discounts } from "../mocks/discounts";
import moment from "moment";
import { formatPrice } from "../utils/formatPrice";

class DiscountsService {
  getDiscounts = () => Promise.resolve(discounts);

  isInDateRange = (date, startDate, endDate) => {
    return startDate && endDate
      ? moment(date).isAfter(startDate) && moment(date).isBefore(endDate)
      : true;
  };

  /* Check if all conditions is accepted */
  isConditionsAccepted = (products, conditions) => {
    const productsObj = products.reduce(
      (acc, cur) => Object.assign(acc, { [cur.id]: cur }),
      {}
    );
    return conditions.every(cond => {
      const product = productsObj[cond.targetProductId];
      return product && product.quantity >= cond.targetProductCount;
    });
  };

  /* Return all discounts which can be applied for input set of products */
  getAvailableDiscounts = (discounts, data) => {
    return discounts.filter(disc => {
      if (

        // Check if product exist in cart
        !data.products.some(prod => prod.id === disc.productId) ||

        // Check if discount has date range and input date is in this range
        !this.isInDateRange(
          data.date,
          disc.attributes.startDate,
          disc.attributes.endDate
        )
      )
        return false;

      // Check additional conditions like exist another necessary product in cart
      if (!disc.attributes.conditions) return true;
      return this.isConditionsAccepted(
        data.products,
        disc.attributes.conditions
      );
    });
  };

  /* Return then most profitable discount for the input product */
  getDiscountForProduct = (product, discounts) => {
    const discountsForProduct =
      discounts && discounts.filter(disc => disc.productId === product.id);

    // Check if we have more than one discount for input product
    if (!discountsForProduct.length > 1) return discountsForProduct[0];
    return discountsForProduct.reduce((acc, disc) => {

      //apply the most profitable discount
      if (!acc || acc.discountAmount <= disc.discountAmount) return disc;
      return acc;
    }, null);
  };

  /* Return applied discounts with summarized data */
  getAppliedDiscounts = (products, discounts) => {
    return products.reduce((appliedDiscounts, product) => {
      const discountForProduct = this.getDiscountForProduct(product, discounts);
      if (discountForProduct) {
        appliedDiscounts.push({
          productId: product.id,
          productName: product.name,
          discountAmount: discountForProduct.discountAmount,
          discount: formatPrice(
            product.price * product.quantity * discountForProduct.discountAmount
          )
        });
      }
      return appliedDiscounts;
    }, []);
  };

  /* Summarize prices for all products with discounts*/
  calculateTotalPrice = (products, discounts) =>
    products.reduce((totalPrice, currentProduct) => {

      // Get discount which will be applied for current product
      const discountForProduct = this.getDiscountForProduct(
        currentProduct,
        discounts
      );

      // Calculate product price and apply discount if exist
      const productPrice = this.calculateProductPrice(
        currentProduct,
        discountForProduct
      );

      // Summarize prices
      return totalPrice + productPrice;
    }, 0);

  /* Summarize prices for all products without discounts */
  calculateSubtotalPrice = products =>
    products.reduce(
      (subtotalPrice, currentProduct) =>
        subtotalPrice + currentProduct.quantity * currentProduct.price,
      0
    );

  /* Calculate price for one product and apply discount if exist */
  calculateProductPrice = (product, discount) => {
    const totalProductPrice = product.price * product.quantity;
    if (!discount) return totalProductPrice;
    return totalProductPrice - totalProductPrice * discount.discountAmount;
  };
}

export const DiscountsServiceInstance = new DiscountsService();
