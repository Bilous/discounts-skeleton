import { DiscountsServiceInstance } from "../DiscountsService";

describe("getDiscountForProduct function", () => {
  it("without discounts", () => {
    const product = { id: 4, name: "Apples", quantity: 5, price: 1 };
    const discounts = [];

    const mostProfitableDiscount = DiscountsServiceInstance.getDiscountForProduct(
      product,
      discounts
    );
    expect(mostProfitableDiscount).toEqual(null);
  });

  it("apply most profitable discount", () => {
    const product = { id: 4, name: "Apples", quantity: 5, price: 1 };

    const discounts = [
      {
        id: 1,
        attributes: {
          startDate: new Date("05/04/2020 00:00:00"),
          endDate: new Date("05/15/2020 00:00:00")
        },
        productId: 4, //apples
        discountAmount: 0.1
      },
      {
        id: 100,
        attributes: {
          startDate: new Date("05/04/2020 00:00:00"),
          endDate: new Date("05/15/2020 00:00:00")
        },
        productId: 4, //apples
        discountAmount: 0.4
      }
    ];

    const mostProfitableDiscount = DiscountsServiceInstance.getDiscountForProduct(
      product,
      discounts
    );
    expect(mostProfitableDiscount).toEqual({
      id: 100,
      attributes: {
        startDate: new Date("05/04/2020 00:00:00"),
        endDate: new Date("05/15/2020 00:00:00")
      },
      productId: 4, //apples
      discountAmount: 0.4
    });
  });
});
