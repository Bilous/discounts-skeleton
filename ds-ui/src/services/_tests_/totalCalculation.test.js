import { DiscountsServiceInstance } from "../DiscountsService";

describe("calculateTotalPrice function", () => {
  const products = [
    {
      id: 4,
      name: "Apples",
      quantity: 5,
      price: 1
    },
    {
      id: 2,
      name: "Bread",
      quantity: 2,
      price: 0.7
    },
    {
      id: 3,
      name: "Milk",
      quantity: 3,
      price: 1.3
    }
  ];

  const discounts = [
    {
      id: 1,
      attributes: {
        startDate: new Date("04/05/2020 00:00:00"),
        endDate: new Date("11/05/2020 00:00:00")
      },
      productId: 4,
      discountAmount: 0.1
    },
    {
      id: 2,
      attributes: {
        conditions: [
          { targetProductId: 4, targetProductCount: 5 },
          { targetProductId: 1, targetProductCount: 2 }
        ]
      },
      productId: 3,
      discountAmount: 0.25
    }
  ];

  const totalPriceExpected = 5 * 1 + 2 * 0.7 + 3 * 1.3;
  const discount = 5 * 1.0 * 0.1 + 3 * 1.3 * 0.25;

  it("total calculation without products and discounts", () => {
    const products = [];
    const discounts = [];
    const totalPrice = DiscountsServiceInstance.calculateTotalPrice(products, discounts);
    expect(totalPrice).toBe(0);
  });

  it("total calculation without discounts", () => {
    const discounts = [];
    const totalPrice = DiscountsServiceInstance.calculateTotalPrice(products, discounts);
    expect(totalPrice).toBe(totalPriceExpected);
  });

  it("total calculation without products", () => {
    const products = [];
    const totalPrice = DiscountsServiceInstance.calculateTotalPrice(products, discounts);
    expect(totalPrice).toBe(0);
  });

  it("total calculation with discounts", () => {
    const totalPrice = DiscountsServiceInstance.calculateTotalPrice(products, discounts);
    expect(totalPrice).toBe(totalPriceExpected - discount);
  });
});
