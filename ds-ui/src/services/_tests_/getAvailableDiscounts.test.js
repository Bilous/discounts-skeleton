import { DiscountsServiceInstance } from "../DiscountsService";

describe("getAvailableDiscounts function", () => {
  it("get available discount with date range", () => {
    const discounts = [
      {
        id: 1,
        attributes: {
          startDate: new Date("04/05/2020 00:00:00"),
          endDate: new Date("11/05/2020 00:00:00")
        },
        productId: 4,
        discountAmount: 0.1
      }
    ];

    const data = {
      date: new Date("06/05/2020 00:00:00"),
      products: [
        {
          id: 4,
          quantity: 5
        }
      ]
    };

    const availableDiscounts = DiscountsServiceInstance.getAvailableDiscounts(discounts, data);
    expect(availableDiscounts).toEqual(discounts);
  });

  it("get available discount with condition", () => {
    const discounts = [
      {
        id: 2,
        attributes: {
          conditions: [{ targetProductId: 1, targetProductCount: 2 }]
        },
        productId: 2,
        discountAmount: 0.5
      }
    ];

    const data = {
      date: new Date("06/05/2020 00:00:00"),
      products: [
        {
          id: 1,
          quantity: 5
        },
        {
          id: 2,
          quantity: 1
        }
      ]
    };

    const availableDiscounts = DiscountsServiceInstance.getAvailableDiscounts(discounts, data);
    expect(availableDiscounts).toEqual(discounts);
  });

  it("get available discount with date range and condition", () => {
    const discounts = [
      {
        id: 1,
        attributes: {
          startDate: new Date("04/05/2020 00:00:00"),
          endDate: new Date("11/05/2020 00:00:00"),
            conditions: [{ targetProductId: 4, targetProductCount: 5 }]
        },
        productId: 4,
        discountAmount: 0.1
      }
    ];

    const data = {
      date: new Date("06/05/2020 00:00:00"),
      products: [
        {
          id: 4,
          quantity: 5
        },
      ]
    };

    const availableDiscounts = DiscountsServiceInstance.getAvailableDiscounts(discounts, data);
    expect(availableDiscounts).toEqual(discounts);
  });
});
