import { DiscountsServiceInstance } from "../DiscountsService";

describe("calculateProductPrice function", () => {
  const product = {
    id: 4,
    name: "Apples",
    quantity: 5,
    price: 1
  };

  const subtotalPrice = 5;

  it("product price calculation without discount", () => {
    const discount = null;

    const productPrice = DiscountsServiceInstance.calculateProductPrice(product, discount);
    expect(productPrice).toBe(subtotalPrice);
  });

  it("product price calculation with discount", () => {
    const discount =
      {
        id: 1,
        attributes: {
          startDate: new Date("04/05/2020 00:00:00"),
          endDate: new Date("11/05/2020 00:00:00")
        },
        productId: 4,
        discountAmount: 0.1
      };

    const discountPrice = 5 * 0.1;

    const productPrice = DiscountsServiceInstance.calculateProductPrice(product, discount);
    expect(productPrice).toBe(subtotalPrice - discountPrice);
  });
});
