import { DiscountsServiceInstance } from "../DiscountsService";

describe("isConditionsAccepted function", () => {
  it("conditions accepted", () => {
    const products = [
      { id: 4, name: "Apples", quantity: 5, price: 1 },
      { id: 1, name: "Soup", quantity: 2, price: 1 }
    ];
    const conditions = [
      { targetProductId: 4, targetProductCount: 5 },
      { targetProductId: 1, targetProductCount: 1 }
    ];

    const isConditionsAccepted = DiscountsServiceInstance.isConditionsAccepted(
      products,
      conditions
    );
    expect(isConditionsAccepted).toBe(true);
  });

  it("conditions declined", () => {
    const products = [
      { id: 4, name: "Apples", quantity: 5, price: 1 },
      { id: 1, name: "Soup", quantity: 2, price: 1 }
    ];
    const conditions = [
      { targetProductId: 1, targetProductCount: 2 },
      { targetProductId: 2, targetProductCount: 1 }
    ];

    const isConditionsAccepted = DiscountsServiceInstance.isConditionsAccepted(
      products,
      conditions
    );
    expect(isConditionsAccepted).toBe(false);
  });
});
