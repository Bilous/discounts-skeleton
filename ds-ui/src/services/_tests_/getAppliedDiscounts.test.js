import { DiscountsServiceInstance } from "../DiscountsService";
import { formatPrice } from "../../utils/formatPrice";

describe("getAppliedDiscounts function", () => {
  const discounts = [
    {
      id: 1,
      attributes: {
        startDate: new Date("04/05/2020 00:00:00"),
        endDate: new Date("11/05/2020 00:00:00")
      },
      productId: 4,
      discountAmount: 0.1
    },
    {
      id: 2,
      attributes: {
        conditions: [{ targetProductId: 1, targetProductCount: 2 }]
      },
      productId: 2,
      discountAmount: 0.5
    }
  ];

  it("get applied discounts", () => {
    const products = [
      {
        id: 4,
        name: "Apples",
        quantity: 5,
        price: 1
      }
    ];

    const expectedDiscounts = [
      {
        productId: 4,
        productName: "Apples",
        discountAmount: 0.1,
        discount: formatPrice(1 * 5 * 0.1)
      }
    ];

    const discountsForProduct = DiscountsServiceInstance.getAppliedDiscounts(
      products,
      discounts
    );
    expect(discountsForProduct).toEqual(expectedDiscounts);
  });

  it("get applied discounts without products", () => {
    const products = [];
    const expectedDiscounts = [];

    const discountsForProduct = DiscountsServiceInstance.getAppliedDiscounts(
      products,
      discounts
    );
    expect(discountsForProduct).toEqual(expectedDiscounts);
  });
});
