import { DiscountsServiceInstance } from "../DiscountsService";

describe("calculateSubtotalPrice function", () => {
  const products = [
    {
      id: 4,
      name: "Apples",
      quantity: 5,
      price: 1
    },
    {
      id: 2,
      name: "Bread",
      quantity: 2,
      price: 0.7
    },
    {
      id: 3,
      name: "Milk",
      quantity: 3,
      price: 1.3
    }
  ];

  const subtotalPriceExpected = 5 * 1 + 2 * 0.7 + 3 * 1.3;

  it("subtotal calculation", () => {
    const subtotalPrice = DiscountsServiceInstance.calculateSubtotalPrice(products);
    expect(subtotalPrice).toBe(subtotalPriceExpected);
  });

  it("subtotal calculation without products", () => {
    const products = [];

    const subtotalPrice = DiscountsServiceInstance.calculateSubtotalPrice(products);
    expect(subtotalPrice).toBe(0);
  });
});
