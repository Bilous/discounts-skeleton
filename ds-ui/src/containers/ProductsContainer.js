import * as React from "react";
import Layout from "../components/Layout/Layout";
import { ProductCard } from "../components/ProductCard/ProductCard";
import { useDispatch, useSelector } from "react-redux";
import { fetchProducts } from "../actions/productsActions";
import { addToCart } from "../actions/cartActions";
import { productsSelector } from "../selectors/productsSelector";

export function ProductsContainer() {
  const dispatch = useDispatch();
  const products = useSelector(productsSelector);

  React.useEffect(() => {
    dispatch(fetchProducts());
  }, []);

  const addToCartHandler = (id, quantity) => {
    dispatch(addToCart(id, quantity));
  };

  return (
    <Layout.Panel>
      <Layout.Header>Products</Layout.Header>
      <Layout.List>
        {products.map((product, index) => (
          <ProductCard
            key={index}
            product={product}
            addToCart={addToCartHandler}
          />
        ))}
      </Layout.List>
    </Layout.Panel>
  );
}
