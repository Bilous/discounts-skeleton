import * as React from "react";
import Layout from "../components/Layout/Layout";
import { useDispatch, useSelector } from "react-redux";
import { fetchDiscounts } from "../actions/discountsActions";
import { discountsSelector } from "../selectors/discountsSelector";
import { DiscountCard } from "../components/DiscountCard/DiscountCard";

export function DiscountsContainer() {
  const dispatch = useDispatch();
  const discounts = useSelector(discountsSelector);

  React.useEffect(() => {
    dispatch(fetchDiscounts());
  }, []);

  return (
    <Layout.Panel>
      <Layout.Header>Discounts</Layout.Header>
      <Layout.List>
        {discounts.map((discount, index) => (
          <DiscountCard key={index} discount={discount} />
        ))}
      </Layout.List>
    </Layout.Panel>
  );
}
