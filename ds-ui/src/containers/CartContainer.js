import * as React from "react";
import Layout from "../components/Layout/Layout";
import { useDispatch, useSelector } from "react-redux";
import { ProductRow, Table, Tbody, Thead } from "../components/Table/Table";
import { discountsSelector } from "../selectors/discountsSelector";
import { fetchDiscounts } from "../actions/discountsActions";
import { fetchProducts } from "../actions/productsActions";
import { TotalCard } from "../components/TotalCard/TotalCard";
import { removeFromCart } from "../actions/cartActions";
import { DiscountsServiceInstance } from "../services/DiscountsService";
import { cartProductsSelector } from "../selectors/cartProductsSelector";

export function CartContainer() {
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(fetchDiscounts());
    dispatch(fetchProducts());
  }, []);

  const discounts = useSelector(discountsSelector);
  const cartProducts = useSelector(cartProductsSelector);

  // Get discounts which can be applied to products in a cart.
  const availableDiscounts = DiscountsServiceInstance.getAvailableDiscounts(
    discounts,
    {
      date: new Date(),
      products: cartProducts
    }
  );

  // Calculate price without discounts.
  const subtotalPrice = DiscountsServiceInstance.calculateSubtotalPrice(
    cartProducts
  );

  // Calculate price with discounts.
  const totalPrice = DiscountsServiceInstance.calculateTotalPrice(
    cartProducts,
    availableDiscounts
  );

  // Get applied discounts with summarized data
  const appliedDiscounts = DiscountsServiceInstance.getAppliedDiscounts(
    cartProducts,
    availableDiscounts
  );

  const removeProductFromCart = id => {
    dispatch(removeFromCart(id));
  };

  return (
    <Layout.Panel>
      <Layout.Header>Cart</Layout.Header>
      {cartProducts.length ? (
        <Layout.TotalContainer>
          <Table>
            <Thead>
              <tr>
                <th>Product</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Total</th>
                <th>Delete</th>
              </tr>
            </Thead>
            <Tbody>
              {cartProducts.map(product => (
                <ProductRow
                  product={product}
                  removeProductFromCart={removeProductFromCart}
                />
              ))}
            </Tbody>
          </Table>
          <TotalCard
            subtotalPrice={subtotalPrice}
            totalPrice={totalPrice}
            appliedDiscounts={appliedDiscounts}
          />
        </Layout.TotalContainer>
      ) : (
        <Layout.EmptyCart />
      )}
    </Layout.Panel>
  );
}
