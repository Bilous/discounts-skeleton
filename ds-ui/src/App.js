import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import { ProductsContainer } from "./containers/ProductsContainer";
import { DiscountsContainer } from "./containers/DiscountsContainer";
import { CartContainer } from "./containers/CartContainer";
import { Navigation } from "./components/Navigation/Navigation";
import { useSelector } from "react-redux";
import { cartItemsQuantitySelector } from "./selectors/cartItemsQunatitySelector";

function App() {
  const totalProductsQuantity = useSelector(cartItemsQuantitySelector);

  return (
    <div className="app">
      <Router>
        <Navigation totalProductQuantity={totalProductsQuantity} />
        <div className="app-main-container">
          <Switch>
            <Route exact path="/">
              <ProductsContainer />
            </Route>
            <Route path="/discounts">
              <DiscountsContainer />
            </Route>
            <Route path="/cart">
              <CartContainer />
            </Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
