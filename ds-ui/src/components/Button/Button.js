import * as React from 'react';
import './Button.css';

export const Button = (props) => {
  return <button className="btn-primary" onClick={props.onClick}>{props.children}</button>
};