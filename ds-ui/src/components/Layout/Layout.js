import * as React from "react";
import "./Layout.css";

export function Layout(props) {
  return <div className="app-layout">{props.children}</div>;
}

const Panel = props => {
  return <div className="app-layout__panel">{props.children}</div>;
};

const Header = props => {
  return <div className="app-layout__header">{props.children}</div>;
};

const List = props => {
  return <div className="app-layout__product-list">{props.children}</div>;
};

const TotalContainer = props => {
  return <div className="app-layout__total-container">{props.children}</div>;
};

const Card = props => {
  return <div className="app-layout__card">{props.children}</div>;
};

const EmptyCart = () => {
  return (
    <div className="app-layout__empty-cart">
      <div>Cart is empty.</div>
      <p>Looks like you have no items in your shopping cart.</p>
    </div>
  );
};

Layout.Panel = Panel;
Layout.Header = Header;
Layout.List = List;
Layout.TotalContainer = TotalContainer;
Layout.Card = Card;
Layout.EmptyCart = EmptyCart;
export default Layout;
