import * as React from "react";
import { icons } from "../Icons/iconsFactory";
import "./ProductCard.css";
import Layout from "../Layout/Layout";
import { appConfig } from "../../appConfig";
import { Button } from "../Button/Button";
import { formatPrice } from "../../utils/formatPrice";

export function ProductCard(props) {
  const { product, addToCart } = props;
  const [quantity, setQuantity] = React.useState(1);

  const onChange = e => {
    setQuantity(parseInt(e.target.value));
  };

  const onClick = () => {
    addToCart({ id: product.id, quantity });
  };

  return (
    <Layout.Card>
      {product.img && (
        <img
          className="product-card__image"
          src={icons[product.img]}
          alt={product.name}
        />
      )}
      <div className="product-card__name">{product.name}</div>
      <div className="product-card__price">
        {formatPrice(product.price)} {appConfig.currencySign}
      </div>
      <div className="product-card__action-bar">
        <input
          className="product-card__quantity-control"
          type="number"
          step={1}
          value={quantity}
          onChange={onChange}
        />
        <Button onClick={onClick}>Add to card</Button>
      </div>
    </Layout.Card>
  );
}
