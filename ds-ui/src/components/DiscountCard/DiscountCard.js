import * as React from "react";
import Layout from "../Layout/Layout";
import "./DiscountCard.css";
import moment from "moment";

export function DiscountCard(props) {
  const { discount } = props;
  return (
    <Layout.Card>
      <div className="discount-card__discount">
        <div>{discount.productName}</div>
        <div>{discount.discountAmount} %</div>
      </div>
      <div className="discount-card__conditions">
        <div>Conditions:</div>
        {discount.attributes.startDate && (
          <div className="discount-card__date">
            From date:{" "}
            {moment(discount.attributes.startDate).format("DD/MM/YYYY")}
          </div>
        )}
        {discount.attributes.endDate && (
          <div className="discount-card__date">
            To date: {moment(discount.attributes.endDate).format("DD/MM/YYYY")}
          </div>
        )}
        {discount.attributes.conditions && (
          <div className="discount-card__conditions-list">
            {discount.attributes.conditions.map((cond, index )=> (
              <li key={`${index}-condition`}>
                {cond.targetProductName} {cond.targetProductCount} pcs
              </li>
            ))}
          </div>
        )}
      </div>
    </Layout.Card>
  );
}