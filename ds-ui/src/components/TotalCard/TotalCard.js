import * as React from "react";
import "./TotalCard.css";
import { appConfig } from "../../appConfig";
import { formatPrice } from "../../utils/formatPrice";

export const TotalCard = props => {
  return (
    <div className="total-card">
      <div className="total-card__subtotal">
        Subtotal:
        <div className="total-card__amount">
          {formatPrice(props.subtotalPrice)} {appConfig.currencySign}
        </div>
      </div>
      <div className="total-card__discount">
        {props.appliedDiscounts.length
          ? props.appliedDiscounts.map((discount, index) => (
              <li key={index} className="total-card__discount-item">
                {discount.productName} {discount.discountAmount}% off:
                <div className="total-card__amount">
                  -{discount.discount} {appConfig.currencySign}
                </div>
              </li>
            ))
          : "(no offers available)"}
      </div>
      <div className="total-card__total">
        Total:
        <div className="total-card__amount">
          {formatPrice(props.totalPrice)} {appConfig.currencySign}
        </div>
      </div>
    </div>
  );
};
