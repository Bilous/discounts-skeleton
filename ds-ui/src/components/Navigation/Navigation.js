import * as React from "react";
import logo from "../../logo.svg";
import { NavLink } from "react-router-dom";
import "./Navigation.css";
import { icons } from "../Icons/iconsFactory";

export const Navigation = props => {
  const { totalProductQuantity } = props;
  return (
    <div className="nav">
      <img src={logo} className="nav-logo" alt="logo" />
      <ul className="nav-menu">
        <li className="nav-menu__item" title="Products">
          <NavLink to="/" activeClassName="nav-menu__item--active" exact={true}>
            <img src={icons.products} alt="Products" />
          </NavLink>
        </li>
        <li className="nav-menu__item" title="Discounts">
          <NavLink to="/discounts" activeClassName="nav-menu__item--active">
            <img src={icons.discounts} alt="Discounts" />
          </NavLink>
        </li>
        <li className="nav-menu__item" title="Cart">
          <NavLink to="/cart" activeClassName="nav-menu__item--active">
            <img src={icons.cart} alt="Cart" />
            {!!totalProductQuantity && (
              <div className="nav-menu__total-quantity">
                {totalProductQuantity}
              </div>
            )}
          </NavLink>
        </li>
      </ul>
    </div>
  );
};
