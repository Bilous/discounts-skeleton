import * as React from "react";
import "./Table.css";
import { icons } from "../Icons/iconsFactory";
import { formatPrice } from "../../utils/formatPrice";
import { appConfig } from "../../appConfig";

export const Table = props => (
  <table className="app-table">{props.children}</table>
);

export const Thead = props => (
  <thead className="app-table__header">{props.children}</thead>
);

export const Tbody = props => (
  <tbody className="app-table__content">{props.children}</tbody>
);

export function ProductRow(props) {
  const { product } = props;
  const removeItem = () => {
    props.removeProductFromCart(product.id);
  };
  return (
    <tr>
      <td>
        <div className="app-table__product-name">
          <img src={icons[product.img]} alt={product.name} />
          <div>{product.name}</div>
        </div>
      </td>
      <td>{product.quantity}</td>
      <td>{formatPrice(product.price)} $</td>
      <td>
        {formatPrice(product.quantity * product.price)} {appConfig.currencySign}
      </td>
      <td className="app-table__product-delete">
        <div className="app-table__delete-icon" onClick={removeItem} />
      </td>
    </tr>
  );
}
