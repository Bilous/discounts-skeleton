import bread from "./bread.svg";
import apples from "./apples.svg";
import soup from "./soup.svg";
import milk from "./milk.svg";
import cart from "./cart.svg";
import products from "./products.svg";
import discounts from "./discounts.svg";
import remove from "./delete.svg";

export const icons = {
  bread: bread,
  apples: apples,
  soup: soup,
  milk: milk,
  cart: cart,
  products: products,
  discounts: discounts,
  remove: remove
};
