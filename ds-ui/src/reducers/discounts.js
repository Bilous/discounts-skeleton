import { handleActions } from "redux-actions";
import { DiscountsActionTypes } from "../actions/discountsActions";

const initialState = {
  items: []
};

export const discountsReducer = handleActions(
  {
    [DiscountsActionTypes.FETCH_DISCOUNTS_SUCCESS]: (state, { payload }) => {
      return { ...state, items: payload };
    }
  },
  initialState
);
