import { handleActions } from "redux-actions";
import { CartActionTypes } from "../actions/cartActions";

const initialState = {
  items: {}
};

export const cartReducer = handleActions(
  {
    [CartActionTypes.ADD_TO_CART]: (state, { payload }) => {
      const { id, quantity } = payload;
      const { items: prevItems } = state;

      if (prevItems[id]) {
        prevItems[id] = prevItems[id] + quantity;
        return { ...state, items: prevItems };
      }

      prevItems[id] = quantity;
      return { ...state, items: prevItems };
    },
    [CartActionTypes.REMOVE_FROM_CART]: (state, { payload }) => {
      const { items: prevItems } = state;
      delete prevItems[payload];
      return {
        ...state,
        items: prevItems
      };
    }
  },
  initialState
);
