import { handleActions } from "redux-actions";
import { ProductActionTypes } from "../actions/productsActions";

const initialState = {
  items: []
};

export const productsReducer = handleActions(
  {
    [ProductActionTypes.FETCH_PRODUCTS_SUCCESS]: (state, { payload }) => {
      return { ...state, items: payload };
    }
  },
  initialState
);
