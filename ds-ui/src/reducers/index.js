import { combineReducers } from "redux";
import { productsReducer } from "./products";
import { cartReducer } from "./cart";
import { discountsReducer } from "./discounts";

export default combineReducers({
  products: productsReducer,
  cart: cartReducer,
  discounts: discountsReducer
});
